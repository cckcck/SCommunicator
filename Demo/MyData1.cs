﻿using System.Text.RegularExpressions;
using SCommunicator;

namespace Demo
{
    /// <summary>
    /// 第一种解析出的数据模型
    /// </summary>
    public class MyData1 : TextAnalyzeResult<int>
    {
        public MyData1()
        {
            this.BeginOfLine = "^&";
            this.EndOfLine = "\r\n";
        }

        public override void Analyze()
        {
            string s = Encoding.GetString(Raw);
            Match m = Regex.Match(s, "\\d+");
            if (m.Success)
            {
                this.Data = int.Parse(m.Value);
                this.Valid = true;
            }
        }
    }
}
